import com.codeborne.selenide.Selenide;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.*;
import page.child.*;

@Listeners(ListenerTest.class)
public class TestPlayground extends BaseTest{

    private final String ajaxLabel = "Data loaded with AJAX get request.";
    private final String clientLabel = "Data calculated on the client side.";
    private final String text= "Input Text";

    @Test(description = "Проверка нажатия кнопки")
    public void checkDinamycId() {
        new IndexPage(BASE_URL)
                .nameTest("Dynamic ID")
                .clickBtn(DynamicIdPage.getButton());
    }

    @Test(description = "Проверка нажатия кнопки и закрытия диалогового окна")
    public void checkClassAttribute() {
        new IndexPage(BASE_URL)
                .nameTest("Class Attribute")
                .clickBtn(ClassAttributePage.getBlueButton())
                .clickAlert();
    }

    @Test(description = "Проверка нажатия кнопки два раза")
    public void checkHidddenLayersPage() {
        new IndexPage(BASE_URL)
                .nameTest("Hidden Layers")
                .clickBtn(HiddenLayersPage.getButton())
                .clickBtn(HiddenLayersPage.getButton());


    }

    @Test(description = "Проверка ожидания текста после нажатия")
    public void checkAjaxDataPage() {
        new IndexPage(BASE_URL)
                .nameTest("AJAX Data")
                .clickBtn(AjaxDataPage.getButton())
                .checkLabel(AjaxDataPage.getLabel(), ajaxLabel, 15);
    }

    @Test(description = "Проверка ожидания текста после нажатия со стороны клиента")
    public void checkClientPage() {
        new IndexPage(BASE_URL)
                .nameTest("Client Side Delay")
                .clickBtn(ClientSideDelayPage.getButton())
                .checkLabel(ClientSideDelayPage.getLabel(), clientLabel, 15);
    }

    @Test(description = "Проверка изменения кнопки и кликабельности после нажатия")
    public void checkClickPage() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Click")
                .clickBtn(ClickPage.getButton())
                .clickBtn(ClickPage.getButton())
                .getAttribute(ClickPage.getButton(), "class"), "btn btn-success");
    }

    @Test(description = "Проверка ввода текста в поле и изменение текста кнопки")
    public void checkInputPage() {
         Assert.assertEquals(new IndexPage(BASE_URL)
                 .nameTest("Text Input")
                 .enterText(TextInputPage.getInput(), text)
                 .clickBtn(TextInputPage.getButton())
                 .getText(TextInputPage.getButton()), text);
    }

    @Test(description = "Проверка прокрутки и нажатие кнопки")
    public void checkScrollbarPage() {
        new IndexPage(BASE_URL)
                .nameTest("Scrollbars")
                .scroll(ScrollbarsPage.getButton())
                .clickBtn(ScrollbarsPage.getButton());
    }

    @Test(description = "Проверка данных в динамической таблице")
    public void checkDynamicTablePage() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Dynamic Table")
                .searchDinamycTable("CPU"),
                new DynamicTablePage().getText(DynamicTablePage.getValueCpuChrome()));
    }

    @Test(description = "Проверка текста в поле")
    public void checkVerifyTextPage() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Verify Text")
                .getText(VerifyTextPage.getFieldText()), "Welcome UserName!");

    }

    @Test(description = "Проверка progressBar")
    public void checkProgressBar() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Progress Bar")
                .clickBtn(ProgressBarPage.getStartButton())
                .waiLoad("75%")
                .clickBtn(ProgressBarPage.getStopButton())
                .getText(ProgressBarPage.getProgressBar()), "75%");
    }

    @Test(description = "Проверка скрытых кнопок")
    public void checkVisibility() {
        new IndexPage(BASE_URL)
                .nameTest("Visibility").clickBtn(VisibilityPage.getButtonHide())
                .visible(VisibilityPage.getButtonHide(), true)
                .visible(VisibilityPage.getButtonOffscreen(), false)
                .visible(VisibilityPage.getButtonRemoved(), false)
                .visible(VisibilityPage.getButtonOtdisplayed(), false)
                .visible(VisibilityPage.getButtonOverlapped(), true)
                .visible(VisibilityPage.getButtonTransparent(), false)
                .visible(VisibilityPage.getButtonZeroWidth(), false)
                .visible(VisibilityPage.getButtonInvisible(), false);

    }

    @Test(description = "Проверка авторизации")
    public void ckeckSampleAppPage() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Sample App")
                .enterText(SampleAppPage.getFieldUserName(), "Alexandr")
                .enterText(SampleAppPage.getFieldPassword(), "pwd")
                .clickBtn(SampleAppPage.getButtonLogIn())
                .getText(SampleAppPage.getButtonLogIn()), "Log Out");

    }

    @Test(description = "Проверка счетчика кликов")
    public void ckeckMouseOverPage() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Mouse Over")
                .clickBtn(MouseOverPage.getHrefText())
                .clickBtn(MouseOverPage.getHrefText())
                .getText(MouseOverPage.getTextCount()), "2");


    }

    @Test(description = "Проверка кнопки (xpath: неразрывный пробел)")
    public void ckeckNonBreakingSpacePage() {
        new IndexPage(BASE_URL)
                .nameTest("Non-Breaking Space")
                .clickBtn(NonBreakingSpacePage.getButton());


    }

    @Test(description = "Проверка ввода текста в поле, которое перекрыто")
    public void checkOverlappedElementPage() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Overlapped Element")
                .scroll(OverlappedElement.getName())
                .enterText(OverlappedElement.getName(), "Alexandr")
                .getVal(OverlappedElement.getName()), "Alexandr");
    }

    @Test(description = "Проверка буфера обмена")
    public void checkShadowDomPage() {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .nameTest("Shadow DOM")
                .clickBtn(ShadowDomPage.getButtonGenerator())
                .clickBtn(ShadowDomPage.getButtonCopy())
                .getVal(ShadowDomPage.getField()),
                Selenide.clipboard().getText());
    }

    @Test(description = "Проверка загрузки страницы")
    public void checkLoadDelayPage() {
        new IndexPage(BASE_URL)
                .nameTest("Load Delay")
                .clickBtn(LoadDelayPage.getButton());
    }



}
