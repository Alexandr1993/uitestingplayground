package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class VisibilityPage extends Page {

    private static final SelenideElement buttonHide = $(By.id("hideButton"));
    private static final SelenideElement buttonRemoved = $(By.id("removedButton"));
    private static final SelenideElement buttonZeroWidth = $(By.id("zeroWidthButton"));
    private static final SelenideElement buttonOverlapped = $(By.id("overlappedButton"));
    private static final SelenideElement buttonTransparent = $(By.id("transparentButton"));
    private static final SelenideElement buttonInvisible = $(By.id("invisibleButton"));
    private static final SelenideElement buttonOffscreen = $(By.id("offscreenButton"));
    private static final SelenideElement buttonOtdisplayed = $(By.id("notdisplayedButton"));

    public static SelenideElement getButtonHide() {
        return buttonHide;
    }

    public static SelenideElement getButtonInvisible() {
        return buttonInvisible;
    }

    public static SelenideElement getButtonOffscreen() {
        return buttonOffscreen;
    }

    public static SelenideElement getButtonOtdisplayed() {
        return buttonOtdisplayed;
    }

    public static SelenideElement getButtonOverlapped() {
        return buttonOverlapped;
    }

    public static SelenideElement getButtonRemoved() {
        return buttonRemoved;
    }

    public static SelenideElement getButtonTransparent() {
        return buttonTransparent;
    }

    public static SelenideElement getButtonZeroWidth() {
        return buttonZeroWidth;
    }

}
