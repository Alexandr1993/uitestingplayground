package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class HiddenLayersPage extends Page {

    private static final SelenideElement button = $(By.id("greenButton"));

    public static SelenideElement getButton() {
        return button;
    }
}
