package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class SampleAppPage extends Page {

    private static final SelenideElement fieldUserName = $(By.name("UserName"));
    private static final SelenideElement fieldPassword = $(By.name("Password"));
    private static final SelenideElement buttonLogIn = $(By.id("login"));

    public static SelenideElement getFieldPassword() {
        return fieldPassword;
    }

    public static SelenideElement getFieldUserName() {
        return fieldUserName;
    }

    public static SelenideElement getButtonLogIn() {
        return buttonLogIn;
    }
}
