package page.child;

import com.codeborne.selenide.SelenideElement;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class DynamicIdPage extends Page {

    private static final SelenideElement button = $(".btn.btn-primary");


    public static SelenideElement getButton() {
        return button;
    }
}
