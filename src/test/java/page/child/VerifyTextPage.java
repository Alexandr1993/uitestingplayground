package page.child;

import com.codeborne.selenide.SelenideElement;
import page.Page;

import static com.codeborne.selenide.Selenide.$x;

public class VerifyTextPage extends Page {

    private static final SelenideElement fieldText = $x("//div/span[contains(text(), \"Welcome\")]");

    public static SelenideElement getFieldText() {
        return fieldText;
    }
}
