package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class ShadowDomPage extends Page {

    private static final SelenideElement field = $(By.id("editField"));
    private static final SelenideElement buttonGenerator = $(By.id("buttonGenerate"));
    private static final SelenideElement buttonCopy = $(By.id("buttonCopy"));

    public static SelenideElement getButtonCopy() {
        return buttonCopy;
    }

    public static SelenideElement getButtonGenerator() {
        return buttonGenerator;
    }

    public static SelenideElement getField() {
        return field;
    }

}
