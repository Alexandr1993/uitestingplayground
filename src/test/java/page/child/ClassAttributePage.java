package page.child;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import page.Page;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class ClassAttributePage extends Page {

    private static final SelenideElement blueButton = $x("//button[contains(@class, \"primary\")]");

    @Override
    public Page clickAlert() {
        Selenide.switchTo().alert().accept();
        return this;
    }

    public static SelenideElement getBlueButton() {
        return blueButton;
    }
}
